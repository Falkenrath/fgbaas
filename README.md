# fgbaas

Distributed thinking emoji api

## Creating a development environment

To have a clean dev environment so that system packages don't affect what you are writing you need to create a
virtual environment. To do this run the following command in your dev directory.
```
python3.8 -m venv venv
```

Activate the venv then install the discord library:
```
source ./venv/bin/activate
pip install -r requirements.txt
```

## Set upstream

To pull code others have contributed you need to set your upstream. To do this run the following:

```
git remote add upstream https://gitlab.com/Falkenrath/fgbaas.git
```

To get the latest code you can run:

```
git pull upstream <branch>
```

## Installing the bot (Not implemented)
```
pip install git+https://gitlab.com/Falkenrath/fgbass.git
```
