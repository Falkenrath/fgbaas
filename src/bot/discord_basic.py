import discord
from discord.ext import commands
from discord.utils import get
import random
import configparser
from os import environ


class FGBClient(discord.Client):
    def __init__(self, bot_command_prefix: str=None, 
                 bot_description: str=None, 
                 token: str=None,
                 filepath: str=None):
        super().__init__()
        self.bot_command_prefix = bot_command_prefix
        self.bot_description = bot_description
        self.token = token

        if not bot_command_prefix:
            self.get_config(filepath)
        self.bot = commands.Bot(command_prefix=bot_command_prefix, 
                                description=bot_description)
        
        self.keywords = {
            'guess': self.guess,
            'usage': self.usage,
            'NoMatch': self.usage
        }
        
        
    def get_config(self, filepath=None):
        if filepath == None:
            filepath = environ.get("FGB_CONFIG_PATH")
        config = configparser.RawConfigParser()
        config.read(filepath)
        for k,v in config.items('DEFAULT'):
            if k in vars(self):
                setattr(self,k,v)


    def run_command(self):
        self.run(self.token)


    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
        
######################################
############ BOT COMMANDS ############
######################################
        
    async def usage(self, message):
        usage = \
f"""```Received {message.content}

Did not match any command.

Valid commands:
{self.bot_command_prefix}guess -- Guessing game```
{self.bot_command_prefix}usage -- output this message
"""
        await message.channel.send(usage)
    
    
    async def guess(self, message):
        await message.channel.send('Guess a number between 1 and 10.')

        def is_correct(m):
            return m.author == message.author and m.content.isdigit()

        answer = random.randint(1, 10)

        try:
            guess = await self.wait_for('message', check=is_correct, timeout=5.0)
        except asyncio.TimeoutError:
            await message.channel.send(f'Sorry, you took too long it was {answer}.')

        if int(guess.content) == answer:
            await message.channel.send('You are right!')
        else:
            await message.channel.send(f'Oops. It is actually {answer}')
            
######################################
######################################
            
    async def on_message(self, message):
        # we do not want the bot to reply to itself
        if message.author.id == self.user.id:
            return
        
        if 'think' in message.content:
            emoji = r'🤔'
            return await message.add_reaction(emoji)

        if message.content.startswith(self.bot_command_prefix):
            # remove the first character and check the first string as the command
            command = message.content.split()[0][1:]
            return await self.keywords.get(command,'NoMatch')(message)
