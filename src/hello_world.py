this_is_a_string = "Hello World"
this_is_an_integer = 1

print(this_is_a_string)

this_is_a_list = [
    this_is_a_string,
    this_is_an_integer
]

this_is_a_dictionary = {
    "integer": 1,
    "string": 'poop dollar',
    'nested_object': this_is_a_list
}

print(this_is_a_dictionary)